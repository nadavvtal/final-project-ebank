/* eslint-disable @typescript-eslint/unbound-method */
import { expect } from "chai";
import validation_service from "../../src/validations/validation.service.js";
import { IAccount } from "../../src/types/types.js";

describe("validation service",  () => { 
    context("#validateNonNegativeAmount", () => {
        it("should be a function", () => {
            expect(validation_service.validateNonNegativeAmount).to.be.a("function");
            expect(validation_service.validateNonNegativeAmount).to.be.instanceOf(Function);
        });
        it("succeeded", ()=> {
            expect(validation_service.validateNonNegativeAmount(100,1234567)).to.eql(true);
        });

        it("failed", () => {
            expect(() => validation_service.validateNonNegativeAmount(-1, 1234567)).throw('account 1234567 has a negative amount');
        });
    })

    context("#accountExist", () => {
        const accout: IAccount = {
            account_id: 1234567,
            currency: 'EUR',
            balance: 6000,
            is_active: true,
            type: 'individual'
        }
        it("should be a function", () => {
            expect(validation_service.accountExist).to.be.a("function");
            expect(validation_service.accountExist).to.be.instanceOf(Function);
        });
        it("succeeded", ()=> {
            expect(validation_service.accountExist(accout,1234567)).to.eql(true);
        });

        it("failed", () => {
            /*@ts-ignore*/
            expect(() => validation_service.accountExist(undefined, 1234567)).throw('account 1234567 does not exist');
        });
    })

    context("#validateActivation", () => {
        it("should be a function", () => {
            expect(validation_service.validateActivation).to.be.a("function");
            expect(validation_service.validateActivation).to.be.instanceOf(Function);
        });
        it("succeeded", ()=> {
            expect(validation_service.validateActivation(true, true, 1234567)).to.eql(true);
            expect(validation_service.validateActivation(false, false, 1234568)).to.eql(true);
        });

        it("failed", () => {
            expect(() => validation_service.validateActivation(true, false, 1234567)).throw('account 1234567 is not deactive');
            expect(() => validation_service.validateActivation(false, true, 1234568)).throw('account 1234568 is not active');
        });
    })


    context("#validateType", () => {
        it("should be a function", () => {
            expect(validation_service.validateType).to.be.a("function");
            expect(validation_service.validateType).to.be.instanceOf(Function);
        });
        it("succeeded", ()=> {
            expect(validation_service.validateType('individual', 'individual', 1234567)).to.eql(true);
            expect(validation_service.validateType('business', 'business', 1234568)).to.eql(true);
            expect(validation_service.validateType('family', 'family', 1234569)).to.eql(true);
        });

        it("failed", () => {
            expect(() => validation_service.validateType('individual', 'family', 1234567)).throw('account 1234567 has invalid type');
            expect(() => validation_service.validateType('family', 'business', 1234568)).throw('account 1234568 has invalid type');
            expect(() => validation_service.validateType('business', 'individual', 1234569)).throw('account 1234569 has invalid type');
        });
    })

    context("#validateCurrency", () => {
        it("should be a function", () => {
            expect(validation_service.validateCurrency).to.be.a("function");
            expect(validation_service.validateCurrency).to.be.instanceOf(Function);
        });
        it("succeeded", ()=> {
            expect(validation_service.validateCurrency('EUR','EUR',false)).to.eql(true);
            expect(validation_service.validateCurrency('EUR','ILS',true)).to.eql(true);
        });

        it("failed", () => {
            expect(() => validation_service.validateCurrency('EUR', 'ILS', false)).throw('currencies does not match');
            expect(() => validation_service.validateCurrency('USD', 'EUR', false)).throw('currencies does not match');
        });
    })

    context("#validateBalanceForTransfer", () => {
        it("should be a function", () => {
            expect(validation_service.validateBalanceForTransfer).to.be.a("function");
            expect(validation_service.validateBalanceForTransfer).to.be.instanceOf(Function);
        });
        it("succeeded", ()=> {
            expect(validation_service.validateBalanceForTransfer(2000, 900,"individual", 1234567)).to.eql(true);
            expect(validation_service.validateBalanceForTransfer(2000, 1000,"individual", 1234567)).to.eql(true);
            expect(validation_service.validateBalanceForTransfer(20000, 9000,"business", 12345678)).to.eql(true);
            expect(validation_service.validateBalanceForTransfer(15000, 5000,"business", 12345678)).to.eql(true);
            expect(validation_service.validateBalanceForTransfer(6000, 1000,"family", 1234567)).to.eql(true);
            expect(validation_service.validateBalanceForTransfer(5500, 400,"family", 1234567)).to.eql(true);
        });
    
        it("failed", ()=> {
            expect(() => validation_service.validateBalanceForTransfer(1200, 1100,"individual", 1234567)).throw('account 1234567 is not allowed to transfer');
            expect(() => validation_service.validateBalanceForTransfer(3000, 2001,"individual", 1234567)).throw('account 1234567 is not allowed to transfer');
            expect(() => validation_service.validateBalanceForTransfer(9000, 100,"business", 12345678)).throw('account 12345678 is not allowed to transfer');
            expect(() => validation_service.validateBalanceForTransfer(11000, 2000,"business", 12345678)).throw('account 12345678 is not allowed to transfer');
            expect(() => validation_service.validateBalanceForTransfer(5200, 300,"family", 1234567)).throw('account 1234567 is not allowed to transfer');
            expect(() => validation_service.validateBalanceForTransfer(7000, 2500,"family", 1234567)).throw('account 1234567 is not allowed to transfer');
        });
    })

    context("#validateAmountLimits", () => {
        it("should be a function", () => {
            expect(validation_service.validateAmountLimits).to.be.a("function");
            expect(validation_service.validateAmountLimits).to.be.instanceOf(Function);
        });

        it("succeeded", ()=> {
            expect(validation_service.validateAmountLimits(100 ,"individual", 1234567)).to.eql(true);
            expect(validation_service.validateAmountLimits(2000 ,"individual", 1234567)).to.eql(true);
            expect(validation_service.validateAmountLimits(500 ,"business", 12345678)).to.eql(true);
            expect(validation_service.validateAmountLimits(1000 ,"business", 12345678)).to.eql(true);
            expect(validation_service.validateAmountLimits(100 ,"family", 1234567)).to.eql(true);
            expect(validation_service.validateAmountLimits(2500 ,"family", 1234567)).to.eql(true);
            expect(validation_service.validateAmountLimits(10000 ,"partners", 12345678)).to.eql(true);
            expect(validation_service.validateAmountLimits(5000 ,"partners", 12345678)).to.eql(true);
        });
    
        it("failed", ()=> {
            expect(() => validation_service.validateAmountLimits(1001 ,"business", 12345678)).throw('account 12345678 exceeded the transfer amount limit');
            expect(() => validation_service.validateAmountLimits(2000 ,"business", 12345678)).throw('account 12345678 exceeded the transfer amount limit');
            expect(() => validation_service.validateAmountLimits(5001 ,"family", 1234567)).throw('account 1234567 exceeded the transfer amount limit');
            expect(() => validation_service.validateAmountLimits(6000 ,"family", 1234567)).throw('account 1234567 exceeded the transfer amount limit');
            expect(() => validation_service.validateAmountLimits(10001 ,"partners", 12345678)).throw('account 12345678 exceeded the transfer amount limit');
            expect(() => validation_service.validateAmountLimits(11000 ,"partners", 12345678)).throw('account 12345678 exceeded the transfer amount limit');
        });
    })

    context("#validatePartners", () => {
        it("should be a function", () => {
            expect(validation_service.validatePartners).to.be.a("function");
            expect(validation_service.validatePartners).to.be.instanceOf(Function);
        });
        it("succeeded", ()=> {
            expect(validation_service.validatePartners(12345678,12345678)).to.eql(true);
            expect(validation_service.validatePartners(12341234,12341234)).to.eql(true);
        });

        it("failed", () => {
            expect(validation_service.validatePartners(12345678,12345677)).to.eql(false);
            expect(validation_service.validatePartners(12341234,12391234)).to.eql(false);
            expect(validation_service.validatePartners(45380340,18497362)).to.eql(false);
        })
    })

    context("#validateInitialAmount", () => {
        it("should be a function", () => {
            expect(validation_service.validateInitialAmount).to.be.a("function");
            expect(validation_service.validateInitialAmount).to.be.instanceOf(Function);
        });

        it("succeeded", () => {
            expect(validation_service.validateInitialAmount(1000,"individual")).to.eql(true);
            expect(validation_service.validateInitialAmount(1500,"individual")).to.eql(true);
            expect(validation_service.validateInitialAmount(10000,"business")).to.eql(true);
            expect(validation_service.validateInitialAmount(20000,"business")).to.eql(true);
            expect(validation_service.validateInitialAmount(5000,"family")).to.eql(true);
            expect(validation_service.validateInitialAmount(6000,"family")).to.eql(true);
        });

        it("failed", () => {
            expect(validation_service.validateInitialAmount(4999,"family")).to.eql(false);
            expect(validation_service.validateInitialAmount(500,"family")).to.eql(false);
        });
        
    });

    context("#isNotFamilyAccount", () => {
        it("should be a function", () => {
            expect(validation_service.isNotFamilyAccount).to.be.a("function");
            expect(validation_service.isNotFamilyAccount).to.be.instanceOf(Function);
        });
        it("succeeded", ()=> {
            expect(validation_service.isNotFamilyAccount('individual',1234567));
            expect(validation_service.isNotFamilyAccount('business',12345678));
        });

        it("failed", () => {
            expect(() => validation_service.isNotFamilyAccount('family', 1234567)).throw('account 1234567 is of type family');
        });
    })
});