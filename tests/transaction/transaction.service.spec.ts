/* eslint-disable @typescript-eslint/unbound-method */
import { expect } from "chai";
import sinon from "sinon";
import validation_controller from "../../src/validations/validation.controller";
import account_service from "../../src/account/account.service";
import { IResponseError } from "../../src/types/result.types";
import transaction_service from "../../src/transaction/transaction.service.js";
import transaction_db_provider from "../../src/transaction/transaction.db.provider.js";
import { ResultSetHeader, RowDataPacket } from "mysql2";
import business_service from "../../src/business/business.service.js";
import account_db_provider from "../../src/account/account.db.provider.js";
import { IAccount, IBusiness } from "../../src/types/types.js";

describe("transaction service", () => {
    const valid_transactions_result = [
        {
            transaction_id: 1,
            src_account_id: 12312312,
            dest_account_id: 12312413,
            amount: 200,
            currency: 'EUR',
            status: 'completed'
        },
        {
            transaction_id: 2,
            src_account_id: 12312312,
            dest_account_id: 12312413,
            amount: 1000,
            currency: 'USD',
            status: 'completed'
        }
    ]

    const invalid_transactions_result = [{}];

    context("#getAllTransactions", () => {
        let get_all_transactions: sinon.SinonStub<[], Promise<RowDataPacket>>;
        before(() => {
            get_all_transactions = sinon.stub(transaction_db_provider, "getAllTransactions");
        })
        after(() => {
            sinon.restore();
        })

        it("should be a function", () => {
            expect(transaction_service.getAllTransactions).to.be.a("function");
            expect(transaction_service.getAllTransactions).to.be.instanceOf(Function);
        });

        it("succeeded", async () => {
            get_all_transactions.resolves(valid_transactions_result as RowDataPacket);
            expect(await transaction_service.getAllTransactions()).to.equal(valid_transactions_result);
        });

        it("failed", async () => {
            get_all_transactions.resolves(invalid_transactions_result as RowDataPacket);
            try {
                await transaction_service.getAllTransactions();
            } catch (error) {
                expect((error as IResponseError).message).to.equal(`no transactions have been made yet`);
            }
        });
    })

    context("#getTransactionsByAccountID", () => {

        let get_transactions_by_account_id: sinon.SinonStub<[account_id: number], Promise<RowDataPacket>>;
        before(() => {
            get_transactions_by_account_id = sinon.stub(transaction_db_provider, "getTransactionsByAccountID");
        })
        after(() => {
            sinon.restore();
        })

        it("should be a function", () => {
            expect(transaction_service.getTransactionsByAccountID).to.be.a("function");
            expect(transaction_service.getTransactionsByAccountID).to.be.instanceOf(Function);
        });

        it("succeeded", async () => {
            get_transactions_by_account_id.resolves(valid_transactions_result as RowDataPacket);
            expect(await transaction_service.getTransactionsByAccountID(12312312)).to.equal(valid_transactions_result);
        });

        it("failed", async () => {
            get_transactions_by_account_id.resolves(invalid_transactions_result as RowDataPacket);
            try {
                await transaction_service.getTransactionsByAccountID(11122233);
            } catch (error) {
                expect((error as IResponseError).message).to.equal(`no transactions have been made yet`);
            }
        });
    })

    context("#createTransfer", () => {

        const business_src_account_B2B = {
            account_id: 3,
            currency: 'EUR',
            balance: 300000,
            is_active: true,
            type: 'business',
            created_at: Date.now(),
            updated_at: Date.now(),
            business_table_id: 1,
            company_id: 12345676,
            company_name: 'HM EU',
            context: 'store',
            address_id: 3
        }

        const business_dest_account_B2B = {
            account_id: 4,
            currency: 'EUR',
            balance: 300000,
            is_active: true,
            type: 'business',
            created_at: Date.now(),
            updated_at: Date.now(),
            business_table_id: 2,
            company_id: 12345678,
            company_name: 'HM IT',
            context: 'store',
            address_id: 4
        }

        const business_src_account_B2I = {
            account_id: 4,
            currency: 'EUR',
            balance: 10000,
            is_active: true,
            type: 'business',
            created_at: Date.now(),
            updated_at: Date.now()
        }

        const individual_dest_account_B2I = {
            account_id: 5,
            currency: 'EUR',
            balance: 10000,
            is_active: true,
            type: 'individual',
            created_at: Date.now(),
            updated_at: Date.now()
        }

        const faimily_src_account_F2B = {
            account_id: 12,
            currency: 'EUR',
            balance: 5200,
            is_active: 1,
            type: 'family',
            created_at: Date.now(),
            updated_at: Date.now()
        }

        const business_dest_account_F2B = {
            account_id: 5,
            currency: 'EUR',
            balance: 80200,
            is_active: 1,
            type: 'business',
            created_at: Date.now(),
            updated_at: Date.now()
        }

        const result_header = {
            fieldCount: 0,
            affectedRows: 2,
            insertId: 0,
            info: 'Rows matched: 2  Changed: 2  Warnings: 0',
            serverStatus: 2,
            warningStatus: 0,
            changedRows: 2
        }

        const result_obj_B2B = {
            src_account_id: 3,
            src_account_balance: 299000,
            src_account_currency: 'EUR',
            dest_account_id: 4,
            dest_account_balance: 301000,
            dest_account_currency: 'EUR'
        }

        const result_obj_B2I = {
            src_account_id: 4,
            src_account_balance: 9500,
            src_account_currency: 'EUR',
            dest_account_id: 5,
            dest_account_balance: 10500,
            dest_account_currency: 'EUR'
        }

        const result_obj_F2B = {
            src_account_id: 12,
            src_account_balance: 5000,
            src_account_currency: 'EUR',
            dest_account_id: 5,
            dest_account_balance: 80400,
            dest_account_currency: 'EUR'
        }

        let get_business_account: sinon.SinonStub<[account_id: number], Promise<IBusiness>>;
        let get_account: sinon.SinonStub<[account_id: number], Promise<IAccount>>;
        let transfer_validations: sinon.SinonStub<[src_account: IAccount | IBusiness, src_given_type: string, amount: number, dest_account: IAccount | IBusiness, dest_given_type: string, fx_flag: boolean], boolean>;
        let update_transfer_balances: sinon.SinonStub<[src_account_id: number, dest_account_id: number, src_balance: number, dest_balance: number], Promise<ResultSetHeader>>;
        let create_transaction: sinon.SinonStub<[src_account_id: number, dest_account_id: number, amount: number, currency: string, status: string], Promise<number>>;

        before(() => {
            get_business_account = sinon.stub(business_service, "getBusinessAccount");
            get_account = sinon.stub(account_service, "getAccount");
            transfer_validations = sinon.stub(validation_controller, "transferValidations");
            update_transfer_balances = sinon.stub(account_db_provider, "updateTransferBalances");
            create_transaction = sinon.stub(transaction_db_provider, "createTransaction");
        })

        after(() => {
            sinon.restore();
        })

        it("should be a function", () => {
            after(() => {
                sinon.restore();
            })
            expect(transaction_service.createTransfer).to.be.a("function");
            expect(transaction_service.createTransfer).to.be.instanceOf(Function);
        });

        it("succeeded B2B", async () => {
            /*@ts-ignore*/
            get_business_account.onFirstCall().resolves(business_src_account_B2B);
            /*@ts-ignore*/
            get_business_account.onSecondCall().resolves(business_dest_account_B2B);
            transfer_validations.returns(true);
            update_transfer_balances.resolves(result_header as ResultSetHeader);
            create_transaction.resolves(3);
            expect(await transaction_service.createTransfer(3, 'business', 4, 'business', 1000, false)).to.deep.equal(result_obj_B2B);
        });

        it("succeeded B2I", async () => {
            /*@ts-ignore*/
            get_account.onFirstCall().resolves(business_src_account_B2I);
            /*@ts-ignore*/
            get_account.onSecondCall().resolves(individual_dest_account_B2I);
            transfer_validations.returns(true);
            update_transfer_balances.resolves(result_header as ResultSetHeader);
            create_transaction.resolves(3);
            expect(await transaction_service.createTransfer(4, 'business', 5, 'individual', 500, false)).to.deep.equal(result_obj_B2I);
        });

        it("succeeded F2B", async () => {
            /*@ts-ignore*/
            get_account.withArgs(12).onFirstCall().resolves(faimily_src_account_F2B);
            /*@ts-ignore*/
            get_account.withArgs(5).onSecondCall().resolves(business_dest_account_F2B);
            transfer_validations.returns(true);
            update_transfer_balances.resolves(result_header as ResultSetHeader);
            create_transaction.resolves(3);
            expect(await transaction_service.createTransfer(12, 'family', 5, 'business', 200, false)).to.deep.equal(result_obj_F2B);
        });
    })
})
