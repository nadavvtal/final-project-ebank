import { expect } from "chai";
import { RowDataPacket } from "mysql2";
import { accountsTypes } from "../../src/types/types";
import { createFullFamilyResult, createShortFamilyResult } from "../../src/utils/family.utils";

describe("family utils", () => {
    const payload = [
        {
            family_account_id: 3,
            family_account_currency: "USD",
            family_account_balance: 10000,
            family_account_is_active: true,
            family_account_type: "family" as accountsTypes,
            family_table_id: 3,
            family_table_context: "travel",
            account_id: 1,
            currency: "USD",
            balance: 10000,
            is_active: true,
            type: "individual" as accountsTypes,
            individual_table_id: 1,
            individual_id: 1234567,
            first_name: "Joe",
            last_name: "Lev",
            email: "joe@gamil.com", 
            address_id: 1,
            country_name: "Israel",
            country_code: 1,
            postal_code: 123456,
            city: "TLV",
            region: "",
            street_name: "Sokolov",
            street_number: 20          
        }, {
            family_account_id: 3,
            family_account_currency: "USD",
            family_account_balance: 10000,
            family_account_is_active: true,
            family_account_type: "family" as accountsTypes,
            family_table_id: 3,
            family_table_context: "travel",
            account_id: 2,
            currency: "USD",
            balance: 10000,
            is_active: true,
            type: "individual" as accountsTypes,
            individual_table_id: 2,
            individual_id: 7654321,
            first_name: "Jane",
            last_name: "Lev",
            email: "jane@gamil.com",  
            address_id: 1,
            country_name: "Israel",
            country_code: 1,
            postal_code: 123456,
            city: "TLV",
            region: "",
            street_name: "Sokolov",
            street_number: 20         
        }
    ]

    const short_result = {
        account_id: 3,
        currency: "USD",
        balance: 10000,
        is_active: true,
        type: "family",
        family_table_id: 3,
        context: "travel",
        owners: [1, 2]
    }

    const full_result = {
        account_id: 3,
        currency: "USD",
        balance: 10000,
        is_active: true,
        type: "family",
        family_table_id: 3,
        context: "travel",
        owners: [
            {
                account_id: 1,
                currency: "USD",
                balance: 10000,
                is_active: true,
                type: "individual" as accountsTypes,
                individual_table_id: 1,
                individual_id: 1234567,
                first_name: "Joe",
                last_name: "Lev",
                email: "joe@gamil.com",  
                address: {
                    address_id: 1,
                    country_name: "Israel",
                    country_code: 1,
                    postal_code: 123456,
                    city: "TLV",
                    region: "",
                    street_name: "Sokolov",
                    street_number: 20         
                }
            },
            {
                account_id: 2,
                currency: "USD",
                balance: 10000,
                is_active: true,
                type: "individual" as accountsTypes,
                individual_table_id: 2,
                individual_id: 7654321,
                first_name: "Jane",
                last_name: "Lev",
                email: "jane@gamil.com",  
                address: {
                    address_id: 1,
                    country_name: "Israel",
                    country_code: 1,
                    postal_code: 123456,
                    city: "TLV",
                    region: "",
                    street_name: "Sokolov",
                    street_number: 20         
                }
            }
        ]
    }

    context("#createShortFamilyResult", () => {
        it("should be a function", () => {
            expect(createShortFamilyResult).to.be.a("function");
            expect(createShortFamilyResult).to.be.instanceOf(Function);
        });

        it("a short family result", () => {
            expect(createShortFamilyResult(payload as RowDataPacket)).to.deep.equal(short_result);
        });


    })

    context("#createFullFamilyResult", () => {
        it("should be a function", () => {
            expect(createFullFamilyResult).to.be.a("function");
            expect(createFullFamilyResult).to.be.instanceOf(Function);
        });

        it("a full family result", () => {
            expect(createFullFamilyResult(payload as RowDataPacket)).to.deep.equal(full_result);
        });
    })
})




