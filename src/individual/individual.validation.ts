// import { onlyLettersAndSpaces } from "../utils/vaildation.utils.js";
// import { Request, Response, NextFunction } from "express";
// import HttpException from "../exceptions/http.exception.js";

// export const validateIndividualSchema = async (
//     req:Request, res: Response, next:NextFunction
// ): Promise<void> => {
//     const { individual_id, first_name, last_name, currency } = req.body;
//     if (
//         !individual_id ||
//         typeof individual_id !== "number" ||
//         !(individual_id >= 1000000 && individual_id <= 9999999)
//     ) {
//         throw new HttpException(400, `invalid payload`);
//     }
//     if (
//         !first_name ||
//         typeof first_name !== "string" ||
//         first_name.length < 2 ||
//         first_name.length > 45 ||
//         !onlyLettersAndSpaces(first_name)
//     ) {
//         throw new HttpException(400, `invalid payload`);
//     }
//     if (
//         !last_name ||
//         typeof last_name !== "string" ||
//         last_name.length < 2 ||
//         last_name.length > 45 ||
//         !onlyLettersAndSpaces(last_name)
//     ) {
//         throw new HttpException(400, `invalid payload`);
//     }
//     if (!currency || typeof currency !== "string") {
//         throw new HttpException(400, `invalid payload`);
//     }
//     next();
// };
