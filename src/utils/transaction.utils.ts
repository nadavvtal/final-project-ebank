import fetch from "node-fetch";
import fs from "fs/promises";
import { TransactionResult } from "../types/result.types.js";

export function createTransactionObjectResult(
    src_id: number,
    src_balance: number,
    src_currency: string,
    dest_id: number,
    dest_balance: number,
    dest_currency: string
): TransactionResult {
    const result_obj = {
        src_account_id: src_id,
        src_account_balance: src_balance,
        src_account_currency: src_currency,
        dest_account_id: dest_id,
        dest_account_balance: dest_balance,
        dest_account_currency: dest_currency,
    };
    return result_obj;
}

export async function getRate(base:string, currency:string):Promise<number>{
  const base_url = `http://api.exchangeratesapi.io/latest`;
  const url = `${base_url}?base=${base}&symbols=${currency}&access_key=b3f837606d81ccfd2d8831891fa8c9b1`;
 
  let response = await fetch(url);
  let json = await response.json();
  if((json as any).rates[currency]) {
    return (json as any).rates[currency];
  }
  else{
    throw new Error(`currency: ${currency} doesn't exist in results`);
  }
}

// log each transaction to transaction.log
export async function transactionsLogs(path: string, transaction_data: TransactionResult, transaction_amount: number, status: string): Promise<void> {
  const newTransaction = `${Date.now()} from: ${transaction_data.src_account_id} to: ${transaction_data.dest_account_id} amount: ${transaction_amount} ${transaction_data.dest_account_currency} status: ${status} \r\n`;
  await fs.appendFile(path, newTransaction);
}