export function onlyLettersAndSpaces(str: string): boolean {
    return /^[A-Za-z\s]*$/.test(str);
}

export function onlyArrayOfNumberTuples(arr: [number, number][]): boolean {
    if (!Array.isArray(arr) || arr.length === 0) {
        return false;
    }
    for (const tuple of arr) {
        if (!Array.isArray(tuple) || tuple.length !== 2) {
            return false;
        }
        if (tuple.some((val) => typeof val !== "number")) {
            return false;
        }
    }
    return true;
}
