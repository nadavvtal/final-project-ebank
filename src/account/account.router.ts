/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-misused-promises */
import express from "express";
// import { vertifyAuthorization } from "../auth/auth.middleware.js";
import raw from "../middleware/route.async.wrapper.js";
import { shape } from "../validations/validation.schema.js";
import account_controller from "./account.controller.js";
import { change_status_schema } from "./account.schema.js";
import { vertifyAuthorization } from "../auth/auth.middleware.js"

const router = express.Router();

// change accounts status
router.patch("/status", raw(vertifyAuthorization(["admin", "super-user"])), shape(change_status_schema), raw(account_controller.changeStatus));
// router.patch("/status", raw(account_controller.changeStatus));

export default router;
