import { accountsTypes, IAccount } from "../types/types.js";
import account_db_provider from "./account.db.provider.js";
import HttpException from "../exceptions/http.exception.js";
import validation_controller from "../validations/validation.controller.js";
import { connection } from "../db/mysql.connection.js";
import config_data from "../config.data.js";
import { AccountsStatusResponse } from "../types/result.types.js";

class AccountService{
    
    async getAccount(account_id: number): Promise<IAccount> {
        const account = await account_db_provider.getAccount(account_id);
        if (!account) {
            throw new HttpException(400, `account ${account_id} does not exsit`);
        }
        return account;
    }

    async createAccount(currency: string, balance: number, type: string): Promise<number> {
        const account_id = await account_db_provider.createAccount(currency, balance, type);
        return account_id;
    }

    async changeStatus(action:string,accounts_ids:[number,accountsTypes][]):Promise<AccountsStatusResponse>{
        const action_bool = action === config_data.account_status.active ? true : false;
        for (const account_id of accounts_ids) {
            const account = await this.getAccount(account_id[0]);
            validation_controller.changeStatusValidations(action_bool, account);
        }

        await connection.beginTransaction();
        try{
            for (const account_id of accounts_ids) {
                await account_db_provider.updateAccountActivation(account_id[0], action_bool);
            }
            await connection.commit();
        }catch(error){
            await connection.rollback();
            throw error;
        }

        const response = {
            accounts: accounts_ids,
            status: action
        }
        return response;
    }
}

const account_service = new AccountService();
export default account_service;