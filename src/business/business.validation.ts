// import { onlyLettersAndSpaces } from "../utils/vaildation.utils.js";
// import { Request, Response,NextFunction } from "express";
// import HttpException from "../exceptions/http.exception.js";

// export const validateBusinessSchema = async (
//     req:Request,res:Response,next:NextFunction
// ): Promise<void> => {
//     const { company_id, company_name, currency } = req.body;
//     if (
//         !company_id ||
//         typeof company_id !== "number" ||
//         !(company_id >= 10000000 && company_id <= 99999999)
//     ) {
//         throw new HttpException(400, `invalid payload`);
//     }
//     if (
//         !company_name ||
//         typeof company_name !== "string" ||
//         company_name.length < 2 ||
//         company_name.length > 45 ||
//         !onlyLettersAndSpaces(company_name)
//     ) {
//         throw new HttpException(400, `invalid payload`);
//     }
//     if (!currency || typeof currency !== "string") {
//         throw new HttpException(400, `invalid payload`);
//     }
//     next();
// };