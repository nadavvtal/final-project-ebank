// import { onlyArrayOfNumberTuples } from "../utils/vaildation.utils.js";
// import { Request, Response,NextFunction } from "express";
// import HttpException from "../exceptions/http.exception.js";

// export const validateCreateFamilyPayload = async (req:Request,res:Response,next:NextFunction) : Promise<void> => {
//     const { currency, owners } = req.body;
//     if (!currency || typeof currency !== "string") {
//         throw new HttpException(400, `invalid payload`);
//     }
//     if ( !owners || !onlyArrayOfNumberTuples(owners as [number, number][])) {
//         throw new HttpException(400, `invalid payload`);
//     }
//     next();
// }

// export const validateIndividualsAccountsPayload = async (req:Request,res:Response,next:NextFunction) : Promise<void> => {
//     const { owners } = req.body;
//     if ( !owners || !onlyArrayOfNumberTuples(owners as [number, number][])) {
//         throw new HttpException(400, `invalid payload`);
//     }
//     next();
// }