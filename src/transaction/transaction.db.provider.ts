import { ResultSetHeader, RowDataPacket } from "mysql2";
import { connection } from "../db/mysql.connection.js";

class TransactionDBProvider {
    async getAllTransactions() {
        const sql_select = "SELECT * FROM transaction";
        const [transactions] = (await connection.query(
            sql_select
        )) as RowDataPacket[];
        return transactions;
    }

    async getTransactionsByAccountID(account_id: number) {
        const sql_select = `SELECT * FROM transaction WHERE src_account_id = "${account_id}" OR dest_account_id = "${account_id}"`;
        const [transactions] = (await connection.query(
            sql_select
        )) as RowDataPacket[];
        return transactions;
    }

    async createTransaction(
        src_account_id: number,
        dest_account_id: number,
        amount: number,
        currency: string,
        status: string
    ): Promise<number> {
        const sql_insert = `INSERT INTO transaction (src_account_id, dest_account_id, amount, currency, status) VALUES (${src_account_id}, ${dest_account_id}, ${amount}, "${currency}", "${status}")`;
        const [transaction] = (await connection.query(
            sql_insert
        )) as ResultSetHeader[];
        return transaction.insertId;
    }
}

const transaction_db_provider = new TransactionDBProvider();
export default transaction_db_provider;
