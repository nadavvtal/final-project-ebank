
export interface ISqlData {
    db_host: string;
    db_port: number;
    db_name: string;
    db_user_name: string;
    db_user_password: string;
}

export interface IAccountsTypes {
    individual: "individual",
    family: "family",
    business: "business",
    partners: "partners"
}

export interface IMinimumInitialAmount {
    individual: 0,
    family: 5000,
    business: 0
}

export interface IMinimumRemainingBalance {
    individual: 1000,
    family: 5000,
    business: 10000
}

export interface ITransferAmountLimit {
    individual: "none",
    family: 5000,
    business: 1000,
    partners: 10000
}

export interface IAccountStatus {
    active: "active",
    deactive: "deactive"
}