import { NextFunction, Request, Response } from "express";
import v4 from "uuid";
const { v4: uuidv4 } = v4;

export const setId = (
    req: Request,
    res: Response,
    next: NextFunction
): void => {
    req.id = uuidv4();
    next();
};
