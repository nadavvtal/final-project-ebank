// Welcome to the TypeScript Playground, this is a website
import { Request, Response,NextFunction } from "express";
import HttpException from "../exceptions/http.exception.js";

class A{
    number(err_msg?:string){
        return (field:any)=>{
            if (!field) return true;
            console.log(field)
        // console.log("in number")
        const condition = typeof(field) === "number";
        if (condition) return true
        if(err_msg){
            throw new HttpException(400,err_msg);  
        }
        throw new HttpException(400,"Error in validat 'number' type");
    
        }
    }

    string(err_msg?:string){
        return (field:any)=>{
            if (!field) return true
            const condition = typeof(field) === "string";
            if (condition) return true
            if(err_msg){
                throw new HttpException(400,err_msg);  
            }else{
                throw new HttpException(400,"Error in validat 'string' type");
            }
        }
    }
    

    mail(err_msg?:string){
        return (field:any)=>{
            if (!field) return true;
            const mail_regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if (field.match(mail_regex)) return true
            if(err_msg){
                throw new HttpException(400,err_msg);  
            }else{
            throw new HttpException(400,"Error in validat mail");
        
            }
        }

    }

    require(err_msg?:string){
        return (field:any)=>{
            if (field) return true
            else{
                if(err_msg){
                    throw new HttpException(400,err_msg);  
                }else{
                throw new HttpException(400,"Error require function");
            
                }
            }
        }
        
    } 

    maxValue(max_value:any,err_msg?:string){
                // console.log("in max_value")
        return (field:any)=>{
            if (!field) return true
            field = Number(field);
            const condition = field <= max_value;
            if (condition) return true;

            if(err_msg){
            throw new HttpException(400,err_msg);  
            }else{
            throw new HttpException(400,"Error in validate max value");
        
            }
        }
    }

    minValue(min_value:any,err_msg?:string){
                // console.log("in min_value")

        return (field:any)=>{
            if (!field) return true;
            field = Number(field);
            const condition = field >= min_value;
            if (condition) return true;
            if(err_msg){
                throw new HttpException(400,err_msg);  
            }else{
            throw new HttpException(400,"Error in validate minvalue");
        
            }


        }
    }

    maxLength(max_length:any,err_msg?:string){
                // console.log("in max_length")
        return (field:any)=>{
            if (!field) return true;
            field = String(field);
            const condition = field.length <= max_length;
            console.log(condition)
            if (condition) return true;


            if(err_msg){
                throw new HttpException(400,err_msg);  
            }else{
            throw new HttpException(400,"Error in validate max length");
        
            }


        }


    }
    minLength(min_length:any,err_msg?:string){
        return (field:any)=>{
            if (!field) return true;
            field = String(field);
            const condition = field.length >= min_length;
            if (condition) return true;


            if(err_msg){
                throw new HttpException(400,err_msg);  
            }else{
            throw new HttpException(400,"Error in validate min length");
        
            }
        }
    }

    shape(model:any){
        return (req:Request,res:Response,next:NextFunction) =>{
            if (req.body.length > model.length){
                throw new HttpException(400,"invalid fields in body");
            }
            const model_keys = Object.keys(model as {});
            const body_keys =  Object.keys(req.body as {});
            const reducer = (previousValue:boolean, currentValue:string ) => previousValue && model_keys.includes(currentValue);
            const checker = body_keys.reduce(reducer,true);
            if (!checker){
                throw new HttpException(400,"invalid fields in body");  
            } 
        
            const body = req.body;
            for (const field in model ){
                const validations = model[field];

                for (const func of validations){
                    body[field] = body[field]||"";
                    func(body[field]);              
                }
            }
            next();
        }

    }

}
// require,type,length, regex,value 
const nso_validator = new A();
export default nso_validator;
// const s = {
//     nadav :[a.require("this field require"),a.string("must be string"),a.minLength(5,"min 5")],
//     ron:[a.require,a.maxValue(100,"max is 100"),a.minValue(60,"min is 60")],
//     check: [a.number()]
// }
// const my_middle= a.shape(s);

// const check = {
//     ron: 80,
//     nadav:"avdsadi",    
// }
// console.log(my_middle(check));
