import CryptoJS from "crypto-js";
import { NextFunction, Request, Response } from "express";
import HttpException from "../exceptions/http.exception.js";
import auth_service from "../auth/auth.service.js";

export async function vertifyAuth(req:Request,res:Response,next:NextFunction):Promise<void>{
    try{
        const req_signature = req.headers["signature"];
        const http_method = req.method.toLowerCase();
        const time_stamp = req.headers["timestamp"];
        const access_key = req.headers["access_key"];   
        const salt = req.headers["salt"];
        const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        let body = req.body;              

        // get secret key from db  
        const secret_key =await auth_service.getSecretKey(access_key as string); 
        console.log(secret_key,access_key)
        const to_sign = http_method + fullUrl + salt + time_stamp + access_key + secret_key + body;
        let signature = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(to_sign, secret_key as string));
        signature = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(signature));
        console.log()
        // validate signature
        if (signature === req_signature){
            console.log("valid")
            next();
        }else{
            console.log("not valid")
            throw new HttpException(401, `Unauthorized`);
        }

    }catch(err){
        next(err);       
    }
}

