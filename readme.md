<h1 align="center">Welcome to NSO bank final project 👋</h1>
[![ts](https://badgen.net/badge/-/node?icon=typescript&label&labelColor=blue&color=555555)](https://github.com/TypeStrong/ts-node)
[<img src="https://img.shields.io/badge/-ESLint-4B32C3.svg?logo=ESlint">](https://eslint.org/)
[![Husky](https://img.shields.io/static/v1?label=Formated+by&message=Prettier&color=ff69b4)](https://github.com/prettier/prettier)
[![Husky](https://img.shields.io/static/v1?label=Hooked+by&message=Husky&color=success)](https://typicode.github.io/husky)
 ![ts](https://badgen.net/badge/-/TypeScript/blue?icon=typescript&label)
 [<img src="https://img.shields.io/badge/-SWC-FECC00.svg?logo=swc">](https://swc.rs/)


<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: RAPYD" src="https://img.shields.io/badge/License-RAPYD-yellow.svg" />
  </a>
</p>

> NSO payments platform 

### 🏠 [Homepage](https://bitbucket.org/shanisabag/final-project-ebank)

## Install

```sh
npm install
```

## Run tests

```sh
npm run test
```

## Author

👤 **Omri Azaria Shany Sabag Nadav tal**

* Github: [@Sany sabag Nadav Tal Omri Azaria ](https://github.com/Sany sabag Nadav Tal Omri Azaria )
* LinkedIn: [@Shany sabag Omri azaria Nadav Tal](https://linkedin.com/in/Shany sabag Omri azaria Nadav Tal)

## Show your support

Give a ⭐️ if this project helped you!
